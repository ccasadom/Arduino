import processing.serial.*; //Importamos la librería Serial

Serial port; //Nombre del puerto serie

int rc=48; //Radio del cuadrado
int[] xc= {100,200,300};  //Posición X

int yc=200;  //Posición Y 

boolean[] e = {false,false,false}; //Situación del led Apagado (false) o encendido (true)
boolean[] s = {false,false,false}; //Estado del mouse si está encima del cuadrado o no

void setup()
{
  println(Serial.list()); //Visualiza los puertos serie disponibles en la consola de abajo
  port = new Serial(this, Serial.list()[1], 9600); //Abre el segundo puerto serie
   
  size(400, 400); 
}

void draw()
{
  background(255,255,255);//Fondo de color blanco
  
  for (int i=0;i<3;i++){
    if (ratonEnCuadrado(i)) {
      s[i] = true;
      stroke(255,0,0);  // Contorno rojo
    } else {
     s[i]=false;  //Si el mouse no está dentro del cuadrado, la variable pasa a ser falsa
     stroke(0,0,0);  //Contorno negro
    }
    if (e[i]==true) {
      switch (i) {
        case 0 : fill(255,0,0); break; // Encendido
        case 1 : fill(0,255,0); break;
        case 2 : fill(0,0,255); 
      }
    } else {
       switch (i) {
        case 0 : fill(100,0,0); break; // Apagado
        case 1 : fill(0,100,0); break;
        case 2 : fill(0,0,100);
      }
    }
    rectMode(RADIUS); //Las coordenadas del cuadrado indican su centro
    rect(xc[i],yc,rc,rc); // Dibujamos el cuadrado
       println(i);
  }

} 
  
void mousePressed()  //Cuando el botón del ratón está apretado
{
  for (int i=0;i<3;i++){
    if (ratonEnCuadrado(i)) {
      e[i]=!e[i]; //El estado cambia
      switch (i) {
        case 0 : port.write("R");; break; 
        case 1 : port.write("G");; break;
        case 2 : port.write("B");; 
      }      
    }
  }
}

boolean ratonEnCuadrado(int c) {
  return (mouseX > xc[c]-rc && mouseX < xc[c]+rc &&  mouseY > yc-rc && mouseY < yc+rc);
}