boolean sR=false; //Estado del led
boolean sG=false;
boolean sB=false;

const int LED_ROJO = 10; // Pin al que está conectado el color rojo
const int LED_VERDE = 11; // Pin al que está conectado el color verde
const int LED_AZUL = 12; // Pin al que está conectado el color azul
 
void setup()
{
  Serial.begin(9600);

  // Inicializamos los LEDs
  pinMode(LED_ROJO, OUTPUT);
  pinMode(LED_VERDE, OUTPUT);
  pinMode(LED_AZUL, OUTPUT);
  digitalWrite(LED_ROJO, HIGH);
  digitalWrite(LED_VERDE, HIGH); 
  digitalWrite(LED_AZUL, HIGH);
}
 
void loop()
{
  if(Serial.available()>0)//Si el Arduino recibe datos a través del puerto serie
  {
    byte dato = Serial.read(); //Los almacena en la variable "dato"
    if(dato==82)  //Si recibe una "R" (en ASCII "82")
    {
      
      sR=!sR; //Cambia el estatus del led
      if (sR) {
        digitalWrite(LED_ROJO, LOW);
      } else {
        digitalWrite(LED_ROJO, HIGH);
      }
    }
    if(dato==71)  //Si recibe una "G" (en ASCII "71")
    {
      sG=!sG; //Cambia el estatus del led
      if (sG) {
        digitalWrite(LED_VERDE, LOW);
      } else {
        digitalWrite(LED_VERDE, HIGH);
      }
    }
    if(dato==66)  //Si recibe una "B" (en ASCII "66")
    {
      sB=!sB; //Cambia el estatus del led
      if (sB) {
        digitalWrite(LED_AZUL, LOW);
      } else {
        digitalWrite(LED_AZUL, HIGH);
      }
    }
  }
}