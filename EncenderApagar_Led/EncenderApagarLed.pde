import processing.serial.*; //Importamos la librería Serial

Serial port; //Nombre del puerto serie

int rc=50; //Radio del cuadrado
int xc=200;  //Posición X del cuadrado
int yc=200;  //Posición Y del cuadrado
boolean encendido = false; //Situación del led Apagado (false) o encendido (true)
boolean sobreC = false; //Estado del mouse si está encima del cuadrado o no

void setup()
{
  println(Serial.list()); //Visualiza los puertos serie disponibles en la consola de abajo
  port = new Serial(this, Serial.list()[1], 9600); //Abre el segundo puerto serie
   
  size(400, 400); 
}

void draw()
{
  background(255,255,255);//Fondo de color blanco
    
  if(mouseX > xc-rc && mouseX < xc+rc &&  mouseY > yc-rc && mouseY < yc+rc) //Si el mouse se encuentra dentro del cuadrado
     {
       sobreC=true;  //Variable que demuestra que el mouse esta dentro del cuadrado
       stroke(255,0,0);  //Contorno rojo
     }
   else
   {
     sobreC=false;  //Si el mouse no está dentro del cuadrado, la variable pasa a ser falsa
     stroke(0,0,0);  //Contorno negro
   }
  if (encendido==true) {
    fill(0,255,0);
  } else {
    fill(0,0,0); // Seleccionamos color de fondo negro
  }
  rectMode(RADIUS); //Las coordenadas del cuadrado indican su centro
  rect(xc,yc,rc,rc); // Dibujamos el cuadrado
} 
  
void mousePressed()  //Cuando el botón del ratón está apretado
{
  if (sobreC==true) //Si el mouse está dentro del cuadrado
  {
    encendido=!encendido; //El estado cambia
    port.write("A"); //Envia una "A" para que el Arduino encienda el led
  }
}
