import processing.serial.*; //Importamos la librería Serial

Serial port; //Nombre del puerto serie
 
int valor;//Valor leído

void setup() {
  println(Serial.list()); //Visualiza los puertos serie disponibles en la consola
  port = new Serial(this, Serial.list()[1], 9600); //Abre el segundo puerto disponible
  
  size(360,360);
  background(0);
  fill(0);
}

void draw() {
  background(0);
  if(port.available() > 0) { // si hay algún dato disponible en el puerto 
     valor = port.read() * 4;//Lee el dato y lo almacena en la variable valor
  }
  if (valor > 200) {
    fill(255,0,0);
  } else if (valor >100) {
    fill(255,255,0);
  } else { 
    fill (0,255,0); 
  }
    
  ellipse(180,180,valor,valor);
}